package main

import (
	"fmt"

	elastic "github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esutil"
)

func main() {

	// ctx := context.Background()
	esClient, err := GetEsClient()
	if err != nil {
		fmt.Println("Error initializing : ", err)
		panic("Client fail ")
	}
	newStudent := Student{
		Name:         "Gopher doe",
		Age:          10,
		AverageScore: 99.9,
	}

	ind, err := esClient.Index("students", esutil.NewJSONReader(&newStudent))

	if err != nil {
		panic(err)
	}
	fmt.Println(ind)
	// fmt.Println("[Elastic][InsertProduct]Insertion Successful")
}

type Student struct {
	Name         string  `json:"name"`
	Age          int     `json:"age"`
	AverageScore float64 `json:"average_score"`
}

func GetEsClient() (*elastic.Client, error) {
	cfg := elastic.Config{
		Addresses: []string{"http://192.168.32.235:9200"},
	}

	client, err := elastic.NewClient(cfg)

	return client, err
}
